<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Films</title>
</head>
<body>
<h1>Films App</h1>
<h2><a href="<c:url value="http://localhost:1841/"/>">Client Application</a></h2>

<br>
<br>
<br>
<h3>REST API</h3>
<a href="<c:url value="/film"/>">Films</a>
<a href="<c:url value="/actor"/>">Actors</a>
</body>
</html>

