Ext.define('IMDb20.model.Actor', {
    extend: 'Ext.data.Model',

    fields: [
        'firstName', 'lastName', 'birthDate', 'gender', 'country'
    ]
});
