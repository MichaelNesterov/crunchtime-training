Ext.define('IMDb20.model.Film', {
    extend: 'Ext.data.Model',

    fields: [
        {
            name: 'id',
        },
        {
            name: 'title',
        },
        {
            name: 'year'
        },
        {
            name: 'budget'
        },
        {
            name: 'genre'
        },
    ]
});
