Ext.define('IMDb20.model.Genre', {
    extend: 'Ext.data.Model',

    fields: [

        { name: 'genreId' },
        { name: 'title' },

    ]
});