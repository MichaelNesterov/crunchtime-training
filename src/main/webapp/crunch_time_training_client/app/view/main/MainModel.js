Ext.define('IMDb20.view.main.MainModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.main',

    data: {
        name: 'IMDb 2.0',
    }
});
