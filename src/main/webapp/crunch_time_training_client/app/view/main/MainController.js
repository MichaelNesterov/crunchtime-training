Ext.define('IMDb20.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.main',

    routes : {
        'films' : 'onFilms',
        'actors' : 'onActors'
    },

    onFilms : function() {
        this.getView().getLayout().setActiveItem(0);
    },

    onActors : function() {
        this.getView().getLayout().setActiveItem(1);
    }
});
