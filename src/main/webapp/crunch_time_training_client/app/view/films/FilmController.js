Ext.define('IMDb20.view.films.FilmController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.film',

    requires: [
        'IMDb20.model.Film'
    ],

    view: 'filmList',

    onItemDeleted: function () {
        var i = 0,
            store =  this.getView().getStore(),
            selected = this.getView().getSelection(),
            ids = [],
            url = 'http://10.10.101.78:8080/film';
        if(selected.length) {
            for (; i < selected.length; i++) {
                ids.push(selected[i].id);
            }

            Ext.Ajax.request({
                url: url,
                method: 'DELETE',
                jsonData: ids,

                success: function (response, opts) {
                    store.reload()
                },
                failure: function (response, opts) {
                    // console.log('server-side failure with status code ' + response.status);
                }
            });
        }
    },

    onItemAdded: function () {
        var rec = Ext.create('IMDb20.model.Film');
        rec.data.id = 0;

        var grid = this.getView();
        var store = grid.getStore();
        var rowEditing = grid.findPlugin('rowediting');

        store.insert(0, rec);
        rowEditing.startEdit(rec, 0);
    },

    onItemEdited: function (editor, e) {
        var record = e.record;
        var method = record.data.id ? 'POST' : 'PUT';
        Ext.Ajax.request({
            url: 'http://10.10.101.78:8080/film',
            method: method,
            jsonData: record.data,
            success: function (response, opts) {
                e.grid.store.reload()
            },
            failure: function (response, opts) {
                // console.log('server-side failure with status code ' + response.status);
            }
        });
    },

    changeLists: function (field, newValue) {
        if (newValue !== 0) {
            var a = field.up('app-main');
            a.getLayout().setActiveItem(newValue);
            field.setValue(0);
        }
    },
});