/**
 * Created by MichaelNesterov on 7/10/2018.
 */
Ext.define('IMDb20.view.films.FilmModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.film',

    data: {
        group: null
    }
});