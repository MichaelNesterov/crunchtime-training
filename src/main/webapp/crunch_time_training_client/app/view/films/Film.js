Ext.define('IMDb20.view.films.Film', {
    extend: 'Ext.grid.Panel',
    xtype: 'filmList',
    controller: 'film',
    viewModel: {
        type: 'film'
    },
    requires: [
        'Ext.form.field.ComboBox',
        'Ext.form.field.Number',
        'Ext.form.field.Text',
        'Ext.grid.feature.Grouping',
        'Ext.grid.plugin.RowEditing',
        'Ext.toolbar.Paging',
        'IMDb20.plugin.CustomGridFilter',
        'IMDb20.store.Film',
        'IMDb20.store.Genre',
        'IMDb20.view.films.FilmController',
        'IMDb20.view.films.FilmModel'
    ],
    title: 'Films',
    listeners: {
        edit: 'onItemEdited'
    },

    plugins: [
        {
            ptype: 'rowediting',
            //clicksToEdit: 1
        },
        'customgridfilter'
    ],

    initComponent: function () {
        var me = this;

        var groupingFeature = Ext.create('Ext.grid.feature.Grouping');

        var groupBox = Ext.create('Ext.form.field.ComboBox', {
            store: [
                {'list': 'title', 'title': 'Title'},
                {'list': 'year', 'title': 'Year'}
            ],
            fieldLabel: 'Grouping',
            bind: {
                value: '{group}',
                myToolTip: '{group}'
            },
            setMyToolTip: function (group) {
              //TODO: apply action
            },
            displayField: 'title',
            valueField: 'list',
            editable: false,
            listeners: {
                change: function(field, newValue) {
                    if(newValue) {
                        me.getStore().setGrouper({
                            property: newValue,
                            direction: 'ASC'
                        });
                    }
                }
            }
        });

        me.store = Ext.create('IMDb20.store.Film', {});
        me.store.on('load', function (store) {

        });

        me.features = [groupingFeature];

        me.selModel = {
            selType: 'checkboxmodel',
            checkOnly: true,
            injectCheckbox: 0,
            showHeaderCheckbox: false
        };

        me.tools = [
            {
                xtype: 'combobox',
                store: [
                    {"list": 0, "title": "Films"},
                    {"list": 1, "title": "Actors"}
                ],
                displayField: 'title',
                valueField: 'list',
                editable: false,
                listeners: {
                    change: 'changeLists'
                }
            }
        ];

        me.tbar = [
            {
                text: 'Add',
                iconCls: 'fa fa-plus',
                handler: 'onItemAdded'
            },
            {
                iconCls: 'x-fa fa-trash',
                tooltip: 'Delete',
                handler: 'onItemDeleted'
            },
            '->',
            groupBox,
            {
                text: 'Clear grouping',
                handler: function() {
                    me.getStore().clearGrouping();
                    groupBox.clearValue();
                }
            },

        ];

        me.columns = [
            {
                text: 'Title',
                dataIndex: 'title',
                flex: 1,
                filter: {
                    type: 'string',
                },
                editor: {
                    xtype: 'textfield',
                    id: 'title'
                }
            },
            {
                text: 'Year',
                dataIndex: 'year',
                flex: 1,
                filter: {
                    type: 'number',
                },
                editor: {
                    xtype: 'numberfield',
                    id: 'year'
                }
            },
            {
                text: 'Budget $',
                dataIndex: 'budget',
                flex: 1,
                filter: {
                    type: 'number',
                },
                editor: {
                    xtype: 'numberfield',
                    id: 'budget'
                }
            },
            {
                text: 'Genres',
                dataIndex: 'genre',
                sortable: false,
                flex: 1,
                editor: {
                    xtype: 'combobox',
                    store: Ext.create('IMDb20.store.Genre'),
                    displayField: 'title',
                    valueField: 'title',
                    editable: false
                }
            }
        ];

        me.on('afterrender', function () {
            //TODO:
            //get, set, add, remove
        }, me);

        me.callParent(arguments);
    },

    bbar: {
        xtype: 'pagingtoolbar',
        displayInfo: true,
        displayMsg: 'Displaying {0} to {1} of {2} &nbsp;records ',
        emptyMsg: "No records to display&nbsp;"
    }
});
