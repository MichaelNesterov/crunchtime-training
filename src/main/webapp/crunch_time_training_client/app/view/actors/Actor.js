Ext.define('IMDb20.view.actors.Actor', {
    extend: 'Ext.grid.Panel',
    xtype: 'actorList',
    controller: 'actor',

    requires: [
        'Ext.form.field.ComboBox',
        'IMDb20.store.Actor',
        'IMDb20.view.actors.ActorController'
    ],

    title: 'Actors',

    store: {
        type: 'actor'
    },

    columns: [
        {text: 'First Name', dataIndex: 'firstName', flex: 1},
        {text: 'Last Name', dataIndex: 'lastName', flex: 1},
        {text: 'Birth Date', dataIndex: 'birthDate', flex: 1},
        {text: 'Gender', dataIndex: 'gender', flex: 1},
        {text: 'Country', dataIndex: 'country', flex: 1}
    ],

    tools: [
        {
            xtype: 'combobox',
            store: [
                {"list": 0, "title": "Films"},
                {"list": 1, "title": "Actors"}
            ],
            displayField: 'title',
            valueField: 'list',
            editable: false,
            listeners: {
                change: 'changeLists',
                afterrender: 'afterrenderCombo'
            }
        }
    ]
});
