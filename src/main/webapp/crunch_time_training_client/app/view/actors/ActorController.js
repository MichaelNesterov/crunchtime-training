Ext.define('IMDb20.view.actors.ActorController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.actor',

    changeLists: function (field, newValue) {
        if(newValue !== 1) {
            var a = field.up('app-main');
            a.getLayout().setActiveItem(newValue);
            field.setValue(1);
        }
    },

    afterrenderCombo: function (field) {
        field.setValue(1);
    }
});