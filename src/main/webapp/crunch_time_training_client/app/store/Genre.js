Ext.define('IMDb20.store.Genre', {
    extend: 'Ext.data.Store',

    requires: [
        'IMDb20.model.Genre'
    ],

    model: 'IMDb20.model.Genre',
    autoLoad: true,
    autoSync: true,

    proxy: {
        type: 'ajax',
        url: 'http://10.10.101.78:8080/genre',
        actionMethods: {
            read: 'GET'
        },
        reader: {
            type: 'json',
            rootProperty: 'genreEntities'
        }
    }
});