Ext.define('IMDb20.store.Actor', {
    extend: 'Ext.data.Store',

    alias: 'store.actor',

    requires: [
        'IMDb20.model.Actor'
    ],

    model: 'IMDb20.model.Actor',
    autoLoad: true,
    autoSync: true,

    proxy: {
        type: 'ajax',
        url: 'http://10.10.101.78:8080/actor',
        reader: {
            type: 'json',
            rootProperty: 'data'
        },
    }
});