Ext.define('IMDb20.store.Film', {
    extend: 'Ext.data.Store',

    alias: 'store.film',

    requires: [
        'IMDb20.model.Film'
    ],

    model: 'IMDb20.model.Film',
    autoLoad: true,
    autoSync: false,
    pageSize: 5,
    remoteSort: true,
    remoteFilter: true,

    proxy: {
        type: 'ajax',
        url: 'http://10.10.101.78:8080/film/paging',
        actionMethods: {
            read: 'GET'
        },
        reader: {
            type: 'json',
            rootProperty: 'filmEntities',
            totalProperty: 'totalCount'
        },
        writer: {
            type: 'json',
            writeAllFields: true
        }
    }
});