Ext.define('IMDb20.plugin.CustomGridFilter', {
    extend: "Ext.AbstractPlugin",
    alias: "plugin.customgridfilter",

    requires: [
        'Ext.form.FieldSet',
        'Ext.form.Panel',
        'Ext.form.field.Number',
        'Ext.form.field.Text',
        'Ext.toolbar.Toolbar'
    ],

    filterOperation: {
        title: 'like',
        year_eq: 'eq',
        year_lt: 'lt',
        year_gt: 'gt',
        budget_eq: 'eq',
        budget_lt: 'lt',
        budget_gt: 'gt',
        genre: 'like'
    },

    filterFieldName: {
        title: 'title',
        year_eq: 'year',
        year_lt: 'year',
        year_gt: 'year',
        budget_eq: 'budget',
        budget_lt: 'budget',
        budget_gt: 'budget',
        genre: 'genre'
    },

    init: function (component) {
        var me = this,
            toolbar = component.down('toolbar[dock=top]'),
            items = [
                '->',
                {
                    text: 'Filter',
                    iconCls: 'fa fa-filter',
                    handler: me.onFilterSelected,
                    scope: me
                }
            ];

        if (toolbar == null) {
            component.addDocked(
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    items: items
                });
        }
        else
            toolbar.add(items);
    },

    onFilterSelected: function () {
        var me = this,
            form = new Ext.form.Panel({
                width: 500,
                height: 400,
                margin: 20,
                bodyPadding: 12,
                title: 'Filter',
                floating: true,
                closable: true,
                scrollable: true,
                items: [
                    {
                        xtype: 'fieldset',
                        title: 'Title',
                        collapsible: true,
                        collapsed: true,
                        items: [{
                            xtype: 'textfield',
                            name: 'title'
                        }]

                    },

                    {
                        xtype: 'fieldset',
                        title: 'Year',
                        collapsible: true,
                        collapsed: true,
                        items: [
                            {
                                xtype: 'numberfield',
                                fieldLabel: '<',
                                name: 'year_lt',
                                minValue: 0,
                                maxValue: 2050,
                            },
                            {
                                xtype: 'numberfield',
                                fieldLabel: '=',
                                name: 'year_eq',
                                minValue: 0,
                                maxValue: 2050
                            },
                            {
                                xtype: 'numberfield',
                                fieldLabel: '>',
                                name: 'year_gt',
                                minValue: 0,
                                maxValue: 2050
                            },
                        ]
                    },

                    {
                        xtype: 'fieldset',
                        title: 'Budget',
                        collapsible: true,
                        collapsed: true,
                        items: [
                            {
                                xtype: 'numberfield',
                                fieldLabel: '<',
                                name: 'budget_lt',
                                minValue: 0,
                                maxValue: 100000000000,
                            },
                            {
                                xtype: 'numberfield',
                                fieldLabel: '=',
                                name: 'budget_eq',
                                minValue: 0,
                                maxValue: 100000000000,
                            },
                            {
                                xtype: 'numberfield',
                                fieldLabel: '>',
                                name: 'budget_gt',
                                minValue: 0,
                                maxValue: 100000000000,
                            },
                        ]
                    }
                ],
                buttons: [
                    {
                        text: 'Add filters',
                        handler: function () {
                            var values = form.getValues(),
                                store = me.getCmp().getStore(),
                                filters = [],
                                i,
                                v;

                            for (i in values) {
                                v = values[i];
                                if (v) {
                                    filters.push(
                                        {
                                            property: me.filterFieldName[i],
                                            value: values[i],
                                            operator: me.filterOperation[i]
                                        });
                                }
                            }

                            store.addFilter(filters);
                        }
                    },
                    {
                        text: 'Remove filters',
                        handler: function () {
                            var store = me.getCmp().getStore();
                            store.clearFilter();
                            store.load();
                        }
                    }
                ]
            })
        ;
        form.show();
    },
})
;