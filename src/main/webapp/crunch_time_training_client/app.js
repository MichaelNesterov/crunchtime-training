Ext.application({
    extend: 'IMDb20.Application',

    name: 'IMDb20',

    requires: [
        'IMDb20.*'
    ],

    mainView: 'IMDb20.view.main.Main'
});
