package by.issoft.cttraining.serializer;

import by.issoft.cttraining.entity.CountryEntity;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

public class CountrySerializer extends JsonSerializer<CountryEntity> {
    @Override
    public void serialize(CountryEntity countryEntity, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeString(countryEntity.getName());
    }
}
