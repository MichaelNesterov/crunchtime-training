package by.issoft.cttraining.serializer;

import by.issoft.cttraining.entity.GenreEntity;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

public class GenreSerializer extends JsonSerializer<GenreEntity> {

    @Override
    public void serialize(GenreEntity s, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeString(s.getTitle());
    }
}
