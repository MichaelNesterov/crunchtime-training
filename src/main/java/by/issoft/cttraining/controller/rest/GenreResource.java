package by.issoft.cttraining.controller.rest;

import by.issoft.cttraining.entity.GenreEntity;
import by.issoft.cttraining.service.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/genre")
public class GenreResource {

    private final GenreService genreService;

    @Autowired
    public GenreResource(GenreService genreService) {
        this.genreService = genreService;
    }

    @GetMapping()
    public @ResponseBody
    List<GenreEntity> findAll() {
        return genreService.getAllGenres();
    }

    @GetMapping("{genreId}")
    public @ResponseBody
    GenreEntity findById(@PathVariable("genreId") int genreId) {
        return genreService.getById(genreId);
    }
}
