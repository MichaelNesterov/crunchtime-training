package by.issoft.cttraining.controller.rest;

import by.issoft.cttraining.dto.FilmDTO;
import by.issoft.cttraining.dto.FilmPageDTO;
import by.issoft.cttraining.dto.FilterDTO;
import by.issoft.cttraining.dto.SorterDTO;
import by.issoft.cttraining.entity.FilmEntity;
import by.issoft.cttraining.service.FilmServices;
import by.issoft.cttraining.util.DtoMapper;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@RestController
@CrossOrigin
@RequestMapping("/film")
public class FilmResource {

    private final FilmServices filmServices;

    @Autowired
    public FilmResource(FilmServices filmServices) {
        this.filmServices = filmServices;
    }

    @GetMapping
    public @ResponseBody
    List<FilmEntity> findAll() {
        return filmServices.selectAllFilms();
    }

    @GetMapping("/paging")
    public @ResponseBody
    FilmPageDTO getFilmsPage(@RequestParam int start,
                             @RequestParam int limit,
                             @RequestParam(required = false) String sort,
                             @RequestParam(required = false) String filter,
                             @RequestParam(required = false) String group) {

        return filmServices.getFilmPageWithSortFilter(start, limit, DtoMapper.sorterMapper(sort, group),  DtoMapper.filterMapper(filter));
    }

    @GetMapping("{filmId}")
    public @ResponseBody
    FilmEntity findById(@PathVariable("filmId") int filmId) {
        return filmServices.selectFilmFromId(filmId);
    }

    @PutMapping
    public void addFilm(@RequestBody FilmDTO filmDTO) {
        filmServices.addFilm(filmDTO);
    }

    @PostMapping
    public void updateFilm(@RequestBody FilmDTO filmDTO) {
        filmServices.updateFilm(filmDTO);
    }

    @DeleteMapping
    public void deleteFilm(@RequestBody List<Long> ids) {
        filmServices.deleteFilm(ids);
    }

}
