package by.issoft.cttraining.controller.rest;

import by.issoft.cttraining.entity.ActorEntity;
import by.issoft.cttraining.service.ActorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/actor")
public class ActorResource {

    private final ActorService actorService;

    @Autowired
    public ActorResource(ActorService actorService) {
        this.actorService = actorService;
    }

    @GetMapping()
    public @ResponseBody
    List<ActorEntity> findAll() {
        return actorService.selectAllActor();
    }

    @GetMapping("{actorId}")
    public @ResponseBody
    ActorEntity findById(@PathVariable("actorId") int actorId) {
        return actorService.selectActorFromId(actorId);
    }

    @PostMapping()
    public @ResponseBody
    ActorEntity addActor(@RequestBody ActorEntity actorEntity) {
        return actorService.insertActor(actorEntity);
    }

}
