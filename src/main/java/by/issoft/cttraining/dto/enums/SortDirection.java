package by.issoft.cttraining.dto.enums;

public enum  SortDirection {

    ASC, DESC;

}
