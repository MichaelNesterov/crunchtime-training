package by.issoft.cttraining.dto.enums;

public enum FilterOperator {
    like, lt, gt, eq
}
