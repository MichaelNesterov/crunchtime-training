package by.issoft.cttraining.dto;

import by.issoft.cttraining.dto.enums.FilterOperator;

public class FilterDTO {
    private FilterOperator operator;
    private Object value;
    private String property;

    public FilterOperator getOperator() {
        return operator;
    }

    public void setOperator(FilterOperator operator) {
        this.operator = operator;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }
}

