package by.issoft.cttraining.dto;

import by.issoft.cttraining.dto.enums.SortDirection;

public class SorterDTO {
    private String property;
    private SortDirection direction;

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public SortDirection getDirection() {
        return direction;
    }

    public void setDirection(SortDirection direction) {
        this.direction = direction;
    }
}
