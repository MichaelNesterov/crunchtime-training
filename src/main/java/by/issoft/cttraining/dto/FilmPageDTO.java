package by.issoft.cttraining.dto;

import by.issoft.cttraining.entity.FilmEntity;

import java.util.List;

public class FilmPageDTO {
    private int totalCount;

    private List<FilmEntity> filmEntities;

    public FilmPageDTO() {
    }

    public FilmPageDTO(int totalCount, List<FilmEntity> filmEntities) {
        this.totalCount = totalCount;
        this.filmEntities = filmEntities;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public List<FilmEntity> getFilmEntities() {
        return filmEntities;
    }

    public void setFilmEntities(List<FilmEntity> filmEntities) {
        this.filmEntities = filmEntities;
    }
}
