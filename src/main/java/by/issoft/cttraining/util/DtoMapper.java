package by.issoft.cttraining.util;

import by.issoft.cttraining.dto.FilterDTO;
import by.issoft.cttraining.dto.SorterDTO;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class DtoMapper {
    public static List<FilterDTO> filterMapper(String filter) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return Objects.isNull(filter) ? new ArrayList<>() :
                    objectMapper.readValue(filter, new TypeReference<List<FilterDTO>>() {
                    });
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static List<SorterDTO> sorterMapper(String sort, String group){
        ObjectMapper objectMapper = new ObjectMapper();
        List<SorterDTO> sorters = null;

        if (!Objects.isNull(group)) {
            sorters = new ArrayList<>();
            try {
                sorters.add(objectMapper.readValue(group, new TypeReference<SorterDTO>() {
                }));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        if (!Objects.isNull(sort)) {
            if(Objects.isNull(sorters)) {
                sorters = new ArrayList<>();
            }
            try {
                sorters.addAll(objectMapper.readValue(sort, new TypeReference<List<SorterDTO>>() {
                }));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        return sorters;
    }
}
