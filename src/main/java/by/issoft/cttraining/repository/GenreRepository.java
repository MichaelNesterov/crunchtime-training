package by.issoft.cttraining.repository;

import by.issoft.cttraining.entity.GenreEntity;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GenreRepository {
    GenreEntity selectById(@Param("id") int id);

    List<GenreEntity> selectAllGenres();
}
