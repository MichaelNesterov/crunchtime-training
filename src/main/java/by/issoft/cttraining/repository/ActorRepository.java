package by.issoft.cttraining.repository;

import by.issoft.cttraining.entity.ActorEntity;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ActorRepository {

    List<ActorEntity> selectAllActor();

    ActorEntity selectActorFromId(@Param("id") int id);

    ActorEntity insertActor(@Param("actorEntity") ActorEntity actorEntity);
}