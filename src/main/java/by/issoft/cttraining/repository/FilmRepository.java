package by.issoft.cttraining.repository;

import by.issoft.cttraining.dto.FilmDTO;
import by.issoft.cttraining.dto.FilterDTO;
import by.issoft.cttraining.dto.SorterDTO;
import by.issoft.cttraining.entity.FilmEntity;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FilmRepository {

    List<FilmEntity> selectAllFilms();

    FilmEntity selectFilmFromId(@Param("id") int id);

    List<FilmEntity> selectFilmPage(@Param("start") int start, @Param("limit") int limit);

    List<FilmEntity> selectFilmPageWithSort(@Param("start") int start, @Param("limit") int limit, @Param("sorters") List<SorterDTO> sorters, @Param("filters") List<FilterDTO> filters);

    int selectFilmCount();

    void updateFilm(FilmDTO filmDTO);

    void addFilm(FilmDTO filmDTO);

    void deleteFilm(@Param("ids") List<Long> ids);
}
