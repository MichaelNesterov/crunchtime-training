package by.issoft.cttraining.service.impl;

import by.issoft.cttraining.entity.ActorEntity;
import by.issoft.cttraining.repository.ActorRepository;
import by.issoft.cttraining.service.ActorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ActorServiceImpl implements ActorService {

    private final ActorRepository actorRepository;

    @Autowired
    public ActorServiceImpl(ActorRepository actorRepository) {
        this.actorRepository = actorRepository;
    }

    @Override
    public List<ActorEntity> selectAllActor() {
        return actorRepository.selectAllActor();
    }

    @Override
    public ActorEntity selectActorFromId(int id) {
        return actorRepository.selectActorFromId(id);
    }

    @Override
    public ActorEntity insertActor(ActorEntity actorEntity) {
        return actorRepository.insertActor(actorEntity);
    }

}