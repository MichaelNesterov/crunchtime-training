package by.issoft.cttraining.service.impl;

import by.issoft.cttraining.dto.FilmDTO;
import by.issoft.cttraining.dto.FilmPageDTO;
import by.issoft.cttraining.dto.FilterDTO;
import by.issoft.cttraining.dto.SorterDTO;
import by.issoft.cttraining.entity.FilmEntity;
import by.issoft.cttraining.repository.FilmRepository;
import by.issoft.cttraining.service.FilmServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class FilmServicesImpl implements FilmServices {

    private final FilmRepository filmRepository;

    @Autowired
    public FilmServicesImpl(FilmRepository filmRepository) {
        this.filmRepository = filmRepository;
    }

    @Override
    public List<FilmEntity> selectAllFilms() {
        return filmRepository.selectAllFilms();
    }

    @Override
    public FilmEntity selectFilmFromId(int id) {
        return filmRepository.selectFilmFromId(id);
    }

    @Override
    public FilmPageDTO getFilmPage(int start, int limit) {
        return new FilmPageDTO(filmRepository.selectFilmCount(), filmRepository.selectFilmPage(start, limit));
    }

    @Override
    public FilmPageDTO getFilmPageWithSortFilter(int start, int limit, List<SorterDTO> sorter, List<FilterDTO> filters) {
        return new FilmPageDTO(filmRepository.selectFilmCount(), filmRepository.selectFilmPageWithSort(start, limit, sorter, filters));
    }

    @Override
    public void addFilm(FilmDTO filmDTO) {
        filmRepository.addFilm(filmDTO);
    }

    @Override
    public void updateFilm(FilmDTO filmDTO) {
        filmRepository.updateFilm(filmDTO);
    }

    @Override
    public void deleteFilm(List<Long> ids) {
        filmRepository.deleteFilm(ids);
    }
}
