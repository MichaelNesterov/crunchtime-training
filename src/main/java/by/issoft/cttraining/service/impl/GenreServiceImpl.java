package by.issoft.cttraining.service.impl;

import by.issoft.cttraining.entity.GenreEntity;
import by.issoft.cttraining.repository.GenreRepository;
import by.issoft.cttraining.service.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GenreServiceImpl implements GenreService {

    private final GenreRepository genreRepository;

    @Autowired
    public GenreServiceImpl(GenreRepository genreRepository) {
        this.genreRepository = genreRepository;
    }

    @Override
    public GenreEntity getById(int id) {
        return genreRepository.selectById(id);
    }

    @Override
    public List<GenreEntity> getAllGenres() {
        return genreRepository.selectAllGenres();
    }
}
