package by.issoft.cttraining.service;

import by.issoft.cttraining.entity.ActorEntity;

import java.util.List;

public interface ActorService {
    List<ActorEntity> selectAllActor();

    ActorEntity selectActorFromId(int id);

    ActorEntity insertActor(ActorEntity actorEntity);
}
