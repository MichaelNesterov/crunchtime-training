package by.issoft.cttraining.service;

import by.issoft.cttraining.dto.FilmDTO;
import by.issoft.cttraining.dto.FilmPageDTO;
import by.issoft.cttraining.dto.FilterDTO;
import by.issoft.cttraining.dto.SorterDTO;
import by.issoft.cttraining.entity.FilmEntity;

import java.util.List;

public interface FilmServices {
    List<FilmEntity> selectAllFilms();

    FilmEntity selectFilmFromId(int id);

    FilmPageDTO getFilmPage(int start, int limit);

    FilmPageDTO getFilmPageWithSortFilter(int start, int limit, List<SorterDTO> sorter, List<FilterDTO> filters);

    void updateFilm(FilmDTO filmDTO);

    void addFilm(FilmDTO filmDTO);

    void deleteFilm(List<Long> ids);
}
