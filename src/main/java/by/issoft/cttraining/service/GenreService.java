package by.issoft.cttraining.service;

import by.issoft.cttraining.entity.GenreEntity;

import java.util.List;

public interface GenreService {
    
    GenreEntity getById(int id);

    List<GenreEntity> getAllGenres();
}
